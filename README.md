# Beamer Sleek Theme

## Getting started

This is a theme for LaTex Beamer. This is not mine, you can find the original one [here](https://www.overleaf.com/latex/templates/sleek-beamer-theme/zzpczkprdbqs).

## Installation

Copy and exectute:

```bash
sudo git clone https://gitlab.com/Puma99/sleektheme.git /usr/local/texlive/texmf-local/tex/latex/sleektheme
```

You may need to update the LaTeX file database by running:

```bash
sudo texhash  
```

## Usage

```latex
\documentclass[compress]{beamer}
% Theme
\usetheme{sleek}
% General presentation settings
\title{Presentation title}
\subtitle{Subtitle of the presentation}
\author{Your name}
\institute{Study area\University {\Medium RheinMain}}

\begin{document}
  % Slides
\end{document}
```

You can find a much more complex example [here](https://gitlab.com/Puma99/plantilla-sleektheme).

## License

Creative Commons CC BY 4.0.
